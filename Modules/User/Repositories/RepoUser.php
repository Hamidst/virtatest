<?php

namespace Modules\User\Repositories;

use App\Common\BaseClasses\BaseRepository;
use App\Common\Interfaces\iDataObject;
use Modules\User\Entities\User;

class RepoUser extends BaseRepository
{

    public function __construct(User $modelUser)
    {
        $this->model = $modelUser;
    }
}
