<?php

namespace Modules\User\Tests\Unit;

use App\Common\Constants\TableNames;
use Illuminate\Support\Facades\Hash;
use Modules\User\Entities\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserDatabaseTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testUserCanBeStored()
    {
        $user = User::factory()->make()->toArray();

        $user['password'] = Hash::make('123456');

        User::factory()->create($user);

        $this->assertDatabaseCount(TableNames::USERS, 1);
        $this->assertDatabaseHas(TableNames::USERS, $user);
    }


}
