<?php

namespace Modules\User\Database\Seeders;

use App\Common\Constants\TableNames;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UsersTableTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table(TableNames::USERS)->insert([
            'name' => 'Hamid',
            'email' => 'hamid.gholami@devolon.fi',
            'password' => bcrypt('123456'),
        ]);
    }
}
