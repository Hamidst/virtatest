<?php

use App\Common\Constants\RouteNames as RouteNamesAlias;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('user')->group(function() :void {
    Route::post('/register', 'HttpStoreUser')->name(RouteNamesAlias::USER_REGISTER);
    Route::post('/login', 'HttpLogin')->name(RouteNamesAlias::USER_LOGIN);
});


Route::group(['prefix' => 'user', 'middleware' => 'auth:api'], function () {
    Route::post('/logout', 'HttpLogOut')->name(RouteNamesAlias::USER_LOGOUT);
});
