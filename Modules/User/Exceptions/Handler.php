<?php

namespace Modules\User\Exceptions;

class Handler
{
    public function render(\Exception $exception)
    {
        if ($exception instanceof ExceptionUserNotFound) {
            return response()->json([
                'message'   => 'User not found',
                'code'      => 1000,
                'message_code'  => 'user_not_found'
            ], 401);
        }
    }
}
