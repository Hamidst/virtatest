<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

/**
 *
 */
class HttpLogOut
{
    /**
     * @return JsonResponse
     */
    public function __invoke()
    {
        $user = Auth::user();

        if (!$user) {
            dd("user not found");
        }

        $user->token()->revoke();

        return response()->json(['result' => true]);
    }
}
