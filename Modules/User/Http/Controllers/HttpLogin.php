<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\User\Exceptions\ExceptionUserNotFound;

/**
 *
 */
class HttpLogin
{
    /**
     * @param Request $request
     * @return Application|ResponseFactory|Response
     * @throws ExceptionUserNotFound
     */
    public function __invoke(Request $request)
    {
        $data = $request->validate([
            'email' => 'email|required',
            'password' => 'required'
        ]);

        if (!auth()->attempt($data)) {
            throw new ExceptionUserNotFound();
        }

        $token = auth()->user()->createToken('VirtaApiToken')->accessToken;

        return response([
            'user' => auth()->user(),
            'token' => $token
        ]);
    }
}
