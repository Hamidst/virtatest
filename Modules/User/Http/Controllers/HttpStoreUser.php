<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Modules\User\Actions\ActionStoreUser;
use Modules\User\Transformers\ResourceUser;
use Modules\User\Validators\ValidatorUser;

/**
 *
 */
class HttpStoreUser
{
    /** @var ActionStoreUser $actionStoreUser */
    private $actionStoreUser;
    /** @var ValidatorUser $validator */
    private $validator;

    /**
     * @param ActionStoreUser $actionStoreUser
     * @param ValidatorUser $validator
     * @param ResourceUser $resourceUser
     */
    public function __construct(ActionStoreUser $actionStoreUser,
                                ValidatorUser   $validator)
    {
        $this->actionStoreUser = $actionStoreUser;
        $this->validator = $validator;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function __invoke(Request $request)
    {
        #1 Validate request
        $this->validator->store($request);

        #2 Store user
        $user = ($this->actionStoreUser)($request);

        #3 Return stored user back
        return response()->json($user->toArray());

    }
}
