<?php

namespace Modules\User\Validators;

use Illuminate\Http\Request;
use Illuminate\Validation\Factory;
use Illuminate\Validation\ValidationException;

/**
 *
 */
class ValidatorUser
{
    /** @var Factory $factory */
    private $factory;

    /**
     * @param Factory $factory
     */
    public function __construct(Factory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * @param Request $request
     * @return void
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $this->factory->validate($request->all(), [
            'name' => 'required',
            'email' => 'email|confirmed|unique:users',
            'password' => 'required'
        ]);
    }
}
