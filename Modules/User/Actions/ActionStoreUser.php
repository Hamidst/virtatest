<?php

namespace Modules\User\Actions;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Modules\User\Repositories\RepoUser;

/**
 *
 */
class ActionStoreUser
{
    /** @var RepoUser $repo_user */
    private $repo_user;

    /**
     * @param RepoUser $repo_user
     */
    public function __construct(RepoUser $repo_user)
    {
        $this->repo_user = $repo_user;
    }

    /**
     * @param Request $request
     * @return void
     */
    public function __invoke(Request $request)
    {
        $data = $request->all();
        $data['password'] = Hash::make($data['password']);
        unset($data['email_confirmation']);
        return $this->repo_user->store($data);
    }
}
