<?php

namespace Modules\Company\Tests\Unit;

use App\Common\Constants\TableNames;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Company\Entities\Company;
use Modules\Station\Entities\Station;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 *
 */
class CompanyDatabaseTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testCompanyCanBeStored()
    {
        $company = Company::factory()->make()->toArray();

        Company::factory()->create($company);

        $this->assertDatabaseCount(TableNames::COMPANIES, 1);
        $this->assertDatabaseHas(TableNames::COMPANIES, $company);
    }

    /**
     * @return void
     */
    public function testCompanyParentRelation()
    {
        $companyParent = Company::factory()->create();
        $companyChild = Company::factory()->create(['parent_company_id' => $companyParent->id]);

        $this->assertInstanceOf(Company::class, $companyChild->parentCompany);
    }

    public function testCompanyChildrenRelation()
    {
        $companyParent = Company::factory()->create();
        $companyChild = Company::factory()->count(5)->create(['parent_company_id' => $companyParent->id]);

        $this->assertEquals(5, count($companyParent->children));
    }

    /**
     * @return void
     */
    public function testCompanyCanHasStations()
    {
        $company = Company::factory()->create();
        $stations = Station::factory()->count(10)->create(['company_id' => $company->id]);

        $this->assertDatabaseCount(TableNames::STATIONS, 10);
    }
}
