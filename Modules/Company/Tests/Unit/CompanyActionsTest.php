<?php

namespace Modules\Company\Tests\Unit;

use App\Common\Constants\TableNames;
use Illuminate\Pagination\LengthAwarePaginator;
use Modules\Company\Actions\ActionGetCompany;
use Modules\Company\Actions\ActionListCompany;
use Modules\Company\Actions\ActionUpdateCompany;
use Modules\Company\DataObjects\DOListCompany;
use Modules\Company\Entities\Company;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 *
 */
class CompanyActionsTest extends TestCase
{

    use RefreshDatabase;
    /**
     * A basic unit test example.
     *
     * @return void
     * @description Assert that we can retrieve an inserted model
     */
    public function testGetCompany()
    {
        $companyArray = Company::factory()->make()->toArray();
        $companyArray['parent_company_id'] = null;
        $companyObject = Company::factory()->create($companyArray);

        $actionGetCompany = resolve(ActionGetCompany::class);
        $actionResult = $actionGetCompany->__invoke($companyObject->id);

        // Assert that getCompanyAction returns the company object
        $this->assertInstanceOf(Company::class, $actionResult);

        //Not necessary assertions
        $this->assertDatabaseHas(TableNames::COMPANIES, $companyArray);
        $this->assertDatabaseCount(TableNames::COMPANIES, 1);
        $this->assertModelExists($companyObject);
    }

    /**
     * @return void
     * @description Check if we store many models we can retrieve them in list method
     */
    public function testListCompany()
    {
        Company::factory()->count(20)->create();

        $DOListCompany = resolve(DOListCompany::class);
        $DOListCompany->prepareFromREquest(request());
        $actionListCompany = resolve(ActionListCompany::class);

        $list = $actionListCompany->__invoke($DOListCompany);

        $this->assertInstanceOf(LengthAwarePaginator::class, $list);
        $this->assertCount(config('site.list_default_per_page'), $list->getCollection());
    }

    /**
     * @return void
     */
    public function testUpdateCompany()
    {
        $companyArray = Company::factory()->make()->toArray();
        $companyModel = Company::factory()->create($companyArray);

        $companyArray['name'] = "new name";
        $actionUpdateCompany = resolve(ActionUpdateCompany::class);

        $result = $actionUpdateCompany->__invoke($companyModel->id, $companyArray);

        $this->assertTrue($result);
    }
}
