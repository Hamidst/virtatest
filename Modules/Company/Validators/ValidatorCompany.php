<?php

namespace Modules\Company\Validators;

use Illuminate\Http\Request;
use Illuminate\Validation\Factory;
use Illuminate\Validation\ValidationException;

/**
 *
 */
class ValidatorCompany
{
    /** @var Factory $factory */
    private $factory;

    /**
     * @param Factory $factory
     */
    public function __construct(Factory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * @param Request $request
     * @return void
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $this->factory->Validate($request->all(),[
            'name'  => 'required|string|min:3',
            'parent_company_id' => 'nullable|numeric'
        ]);
    }

    public function update(Request $request)
    {
        $this->factory->Validate($request->all(),[
            'name'  => 'string|min:3',
            'parent_company_id' => 'nullable|numeric'
        ]);
    }
}
