<?php

namespace Modules\Company\Actions;

use Modules\Company\Repositories\RepoCompany;

/**
 *
 */
class ActionUpdateCompany
{

    /** @var RepoCompany $repoCompany */
    private $repoCompany;

    /**
     * @param RepoCompany $repoCompany
     */
    public function __construct(RepoCompany $repoCompany)
    {
        $this->repoCompany = $repoCompany;
    }

    /**
     * @param int $companyId
     * @param array $newCompany
     * @return bool|int
     */
    public function __invoke(int $companyId, array $newCompany)
    {
        return $this->repoCompany->update($companyId, $newCompany);
    }
}
