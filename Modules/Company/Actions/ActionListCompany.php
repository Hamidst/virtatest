<?php

namespace Modules\Company\Actions;

use App\Common\Interfaces\iDataObject;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Modules\Company\Repositories\RepoCompany;

/**
 *
 */
class ActionListCompany
{
    /** @var RepoCompany $repoCompany */
    private $repoCompany;

    /**
     * @param RepoCompany $repoCompany
     */
    public function __construct(RepoCompany $repoCompany)
    {
        $this->repoCompany = $repoCompany;
    }

    /**
     * @param iDataObject $dataObject
     * @return LengthAwarePaginator|Builder[]|Collection
     */
    public function __invoke(iDataObject $dataObject)
    {
        return $this->repoCompany->list($dataObject);
    }
}
