<?php

namespace Modules\Company\Actions;

use Illuminate\Http\Request;
use Modules\Company\Repositories\RepoCompany;

class ActionStoreCompany
{
    /** @var RepoCompany $repoCompany */
    private $repoCompany;

    public function __construct(RepoCompany $repoCompany)
    {
        $this->repoCompany = $repoCompany;
    }

    public function __invoke(Request $request)
    {
        return $this->repoCompany->store($request->all());
    }
}
