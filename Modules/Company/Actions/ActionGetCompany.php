<?php

namespace Modules\Company\Actions;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Modules\Company\Repositories\RepoCompany;

/**
 *
 */
class ActionGetCompany
{
    /** @var RepoCompany $repoCompany */
    private $repoCompany;

    /**
     * @param RepoCompany $repoCompany
     */
    public function __construct(RepoCompany $repoCompany)
    {
        $this->repoCompany = $repoCompany;
    }

    /**
     * @param int $id
     * @param array $with
     * @return Builder|Model|null
     */
    public function __invoke(int $id, array $with = [])
    {
        return $this->repoCompany->get($id, $with);
    }
}
