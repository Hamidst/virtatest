<?php

namespace Modules\Company\Exceptions;

class Handler
{
    public function render(\Exception $exception)
    {
        if ($exception instanceof BaseCompanyException) {
            return response()->json([
                'message'   => 'Company not found',
                'code'      => 3000,
                'message_code'  => 'company_not_found'
            ], 204);
        }
    }
}
