<?php

namespace Modules\Company\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Company\Database\factories\CompanyFactory;
use Modules\Station\Entities\Station;


/**
 *
 */
class Company extends Model
{
    use HasFactory;

    /**
     * @var string[]
     */
    protected $fillable = [
        'parent_company_id',
        'name',
    ];

    /**
     * @return HasMany
     */
    public function stations()
    {
        return $this->hasMany(Station::class);
    }

    public function parentCompany()
    {
        return $this->belongsTo(self::class, 'parent_company_id');
    }

    private function subCompanies()
    {
        return $this->hasMany(self::class, 'parent_company_id', 'id');
    }

    public function children()
    {
        return $this->subCompanies()->with(['children', 'children.stations']);
    }

    protected static function newFactory()
    {
        return CompanyFactory::new();
    }
}
