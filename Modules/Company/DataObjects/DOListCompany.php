<?php

namespace Modules\Company\DataObjects;

use App\Common\BaseClasses\BaseDataObject;
use Illuminate\Http\Request;
use PhpParser\Node\Expr\Cast\Double;

class DOListCompany extends BaseDataObject
{
    public function prepareFromRequest(Request $request)
    {
        parent::prepareFromRequest($request);

        $this->setWith(['stations', 'parentCompany', 'children', 'children.stations']);

        return $this;
    }
}
