<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'company', 'middleware' => 'auth:api'], function () {
    Route::post('/', "HttpStoreCompany");
});

Route::group(['prefix' => 'company'], function () {
    Route::get('/{id}', "HttpGetCompany")->where('id', '[0-9]+');
    Route::put('/{id}', "HttpUpdateCompany")->where('id', '[0-9]+');
    Route::get('/', "HttpListCompany");
});
