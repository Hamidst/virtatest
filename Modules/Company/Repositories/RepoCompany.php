<?php

namespace Modules\Company\Repositories;

use App\Common\BaseClasses\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Modules\Company\Entities\Company;

/**
 *
 */
class RepoCompany extends BaseRepository
{
    /**
     * @param Company $modelCompany
     */
    public function __construct(Company $modelCompany)
    {
        $this->model = $modelCompany;
    }

    /**
     * @param int $id
     * @param array $with
     * @return Builder|Model|null
     */
    public function get(int $id, array $with = [])
    {
        return $query = $this->model->newQuery()
            ->where('id', $id)
            ->with($with)
            ->first();
    }
}
