<?php

namespace Modules\Company\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Modules\Company\Actions\ActionStoreCompany;
use Modules\Company\Validators\ValidatorCompany;

/**
 *
 */
class HttpStoreCompany
{
    /** @var ActionStoreCompany $actionStoreCompany */
    private $actionStoreCompany;
    /** @var ValidatorCompany $validator */
    private $validator;

    /**
     * @param ActionStoreCompany $actionStoreCompany
     * @param ValidatorCompany $validator
     */
    public function __construct(ActionStoreCompany $actionStoreCompany, ValidatorCompany $validator)
    {
        $this->actionStoreCompany = $actionStoreCompany;
        $this->validator = $validator;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function __invoke(Request $request)
    {
        #1 Validation
        $this->validator->store($request);

        #2 store company
        $company = $this->actionStoreCompany->__invoke($request);

        #3 return stored company back to user
        return response()->json($company->toArray());
    }
}
