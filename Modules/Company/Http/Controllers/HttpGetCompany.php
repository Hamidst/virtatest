<?php

namespace Modules\Company\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Modules\Company\Actions\ActionGetCompany;
use Modules\Company\Exceptions\ExceptionCompanyNotFound;

/**
 *
 */
class HttpGetCompany
{
    /** @var ActionGetCompany $actionGetCompany */
    private $actionGetCompany;

    /**
     * @param ActionGetCompany $actionGetCompany
     */
    public function __construct(ActionGetCompany $actionGetCompany)
    {
        $this->actionGetCompany = $actionGetCompany;
    }

    /**
     * @param int $id
     * @return JsonResponse
     * @throws ExceptionCompanyNotFound
     * @description This method is being used to fetch a company and it's stations and it's children's stations if needed.
     */
    public function __invoke(int $id)
    {
        $company = $this->actionGetCompany->__invoke($id, ['stations', 'children.stations']);

        if (!$company) {
            throw new ExceptionCompanyNotFound();
        }

        return response()->json($company->toArray());
    }
}
