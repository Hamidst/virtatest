<?php

namespace Modules\Company\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Modules\Company\Actions\ActionUpdateCompany;
use Modules\Company\Exceptions\ExceptionCompanyNotFound;
use Modules\Company\Validators\ValidatorCompany;

/**
 *
 */
class HttpUpdateCompany
{
    /** @var ActionUpdateCompany $actionUpdateCompany */
    private $actionUpdateCompany;
    /** @var ValidatorCompany $validator */
    private $validator;

    /**
     * @param ActionUpdateCompany $actionUpdateCompany
     * @param ValidatorCompany $validator
     */
    public function __construct(ActionUpdateCompany $actionUpdateCompany, ValidatorCompany $validator)
    {
        $this->actionUpdateCompany = $actionUpdateCompany;
        $this->validator = $validator;
    }

    /**
     * @param int $companyId
     * @return JsonResponse
     * @throws ExceptionCompanyNotFound
     */
    public function __invoke(int $companyId)
    {
        $this->validator->update(\request());

        if (!$this->actionUpdateCompany->__invoke($companyId, \request()->all())) {
            throw new ExceptionCompanyNotFound();
        }

        return response()->json(['result' => true]);
    }
}
