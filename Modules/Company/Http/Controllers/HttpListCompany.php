<?php

namespace Modules\Company\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Modules\Company\Actions\ActionListCompany;
use Modules\Company\DataObjects\DOListCompany;

/**
 *
 */
class HttpListCompany
{
    /** @var DOListCompany $DOListCompany */
    private $DOListCompany;
    /** @var ActionListCompany $actionListCompany */
    private $actionListCompany;

    /**
     * @param DOListCompany $DOListCompany
     * @param ActionListCompany $actionListCompany
     */
    public function __construct(DOListCompany $DOListCompany, ActionListCompany $actionListCompany)
    {
        $this->DOListCompany = $DOListCompany;
        $this->actionListCompany = $actionListCompany;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @description This method is being used to return list of companies in paginated or without pagination form
     * @sample-output
     * {
        "current_page": 1,
        "data": [
            {
                "id": 1,
                "parent_company_id": null,
                "name": "companyA",
                "created_at": "2022-03-24T12:41:13.000000Z",
                "updated_at": "2022-03-24T12:41:13.000000Z"
            },
            {
                "id": 2,
                "parent_company_id": 1,
                "name": "companyB",
                "created_at": "2022-03-24T12:41:19.000000Z",
                "updated_at": "2022-03-24T12:41:19.000000Z"
            },
            {
                "id": 3,
                "parent_company_id": 2,
                "name": "companyC",
                "created_at": "2022-03-24T12:41:33.000000Z",
                "updated_at": "2022-03-24T12:41:33.000000Z"
            },
            {
                "id": 4,
                "parent_company_id": 3,
                "name": "companyD",
                "created_at": "2022-03-24T12:41:38.000000Z",
                "updated_at": "2022-03-24T12:41:38.000000Z"
            }
        ],
        "first_page_url": "http://0.0.0.0:8000/api/company?Page=1",
        "from": 1,
        "last_page": 1,
        "last_page_url": "http://0.0.0.0:8000/api/company?Page=1",
        "links": [
            {
                "url": null,
                "label": "&laquo; Previous",
                "active": false
            },
            {
                "url": "http://0.0.0.0:8000/api/company?Page=1",
                "label": "1",
                "active": true
            },
            {
                "url": null,
                "label": "Next &raquo;",
                "active": false
            }
        ],
        "next_page_url": null,
        "path": "http://0.0.0.0:8000/api/company",
        "per_page": 10,
        "prev_page_url": null,
        "to": 4,
        "total": 4
    }
     */
    public function __invoke(Request $request)
    {
        $companies = ($this->actionListCompany)($this->DOListCompany->prepareFromRequest($request));

        return response()->json($companies->toArray());
    }
}
