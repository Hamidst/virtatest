<?php

namespace Modules\Company\Database\Seeders;

use App\Common\Constants\TableNames;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CompaniesTableTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table(TableNames::COMPANIES)->insert([
            'name' => 'CompanyA',
            'parent_company_id' => null
        ]);

        DB::table(TableNames::COMPANIES)->insert([
            'name' => 'CompanyB',
            'parent_company_id' => null
        ]);

        DB::table(TableNames::COMPANIES)->insert([
            'name' => 'CompanyAA',
            'parent_company_id' => 1
        ]);

        DB::table(TableNames::COMPANIES)->insert([
            'name' => 'CompanyBB',
            'parent_company_id' => 2
        ]);
    }
}
