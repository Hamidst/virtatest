<?php

namespace Modules\Station\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\Company\Entities\Company;
use Modules\Station\Database\factories\StationFactory;

/**
 *
 */
class Station extends Model
{
    use HasFactory;

    /**
     * @var string[]
     */
    protected $fillable = [
        'company_id',
        'name',
        'latitude',
        'longitude',
        'address'
    ];

    /**
     * @return BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    protected static function newFactory()
    {
        return StationFactory::new();
    }
}
