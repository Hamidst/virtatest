<?php

namespace Modules\Station\Repositories;

use App\Common\BaseClasses\BaseRepository;
use App\Common\Interfaces\iDataObject;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Modules\Station\Entities\Station;

/**
 *
 */
class RepoStation extends BaseRepository
{
    /**
     * @param Station $modelStation
     */
    public function __construct(Station $modelStation)
    {
        $this->model = $modelStation;
    }

    /**
     * @param iDataObject $dataObject
     * @return array|Builder[]
     */
    public function list(iDataObject $dataObject)
    {
        $result = DB::select($dataObject->getRawQuery());

        return $result;
    }
}
