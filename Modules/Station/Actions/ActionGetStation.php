<?php

namespace Modules\Station\Actions;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Modules\Station\Repositories\RepoStation;

/**
 *
 */
class ActionGetStation
{
    /** @var RepoStation $repoStation */
    private $repoStation;

    /**
     * @param RepoStation $repoStation
     */
    public function __construct(RepoStation $repoStation)
    {
        $this->repoStation = $repoStation;
    }

    /**
     * @param int $stationId
     * @return Builder|Builder[]|Collection|Model|null
     */
    public function __invoke(int $stationId)
    {
        return $this->repoStation->get($stationId);
    }
}
