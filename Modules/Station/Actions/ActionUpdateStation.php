<?php

namespace Modules\Station\Actions;

use Modules\Station\Repositories\RepoStation;

/**
 *
 */
class ActionUpdateStation
{
    /** @var RepoStation $repoStation */
    private $repoStation;

    /**
     * @param RepoStation $repoStation
     */
    public function __construct(RepoStation $repoStation)
    {
        $this->repoStation = $repoStation;
    }

    /**
     * @param int $stationId
     * @param array $station
     * @return bool|int
     */
    public function __invoke(int $stationId, array $station)
    {
        return $this->repoStation->update($stationId, $station);
    }
}
