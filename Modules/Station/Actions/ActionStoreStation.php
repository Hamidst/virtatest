<?php

namespace Modules\Station\Actions;

use Illuminate\Http\Request;
use Modules\Station\Repositories\RepoStation;

/**
 *
 */
class ActionStoreStation
{
    /** @var RepoStation $repoStation */
    private $repoStation;

    /**
     * @param RepoStation $repoStation
     */
    public function __construct(RepoStation $repoStation)
    {
        $this->repoStation = $repoStation;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function __invoke(Request $request)
    {
        $data = $request->all();
        return $this->repoStation->store($data);
    }
}
