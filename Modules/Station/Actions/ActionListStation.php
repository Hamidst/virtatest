<?php

namespace Modules\Station\Actions;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Modules\Station\DataObjects\DOListStation;
use Modules\Station\Repositories\RepoStation;

/**
 *
 */
class ActionListStation
{
    /** @var RepoStation $repoStation */
    private $repoStation;

    /**
     * @param RepoStation $repoStation
     */
    public function __construct(RepoStation $repoStation)
    {
        $this->repoStation = $repoStation;
    }

    /**
     * @param DOListStation $DOListStation
     * @return LengthAwarePaginator|Builder[]|Collection
     */
    public function __invoke(DOListStation $DOListStation)
    {
        return $this->repoStation->list($DOListStation);
    }
}
