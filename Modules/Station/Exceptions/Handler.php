<?php

namespace Modules\Station\Exceptions;

class Handler
{
    public function render(\Exception $exception)
    {
        if ($exception instanceof BaseStationException) {
            return response()->json([
                'message'   => 'Station not found',
                'code'      => 2000,
                'message_code'  => 'station_not_found'
            ], 204);
        }
    }
}
