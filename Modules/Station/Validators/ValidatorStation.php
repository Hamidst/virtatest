<?php

namespace Modules\Station\Validators;

use Illuminate\Http\Request;
use Illuminate\Validation\Factory;
use Illuminate\Validation\ValidationException;

/**
 *
 */
class ValidatorStation
{
    /** @var Factory $factory */
    private $factory;

    /**
     * @param Factory $factory
     */
    public function __construct(Factory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * @param Request $request
     * @return void
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $this->factory->validate($request->all(), [
            'latitude'  => 'required',
            'longitude'  => 'required',
            'company_id'    => 'required|numeric',
            'address'    => 'required|min:4',
            'name'    => 'required|min:3'
        ]);
    }

    /**
     * @param Request $request
     * @return void
     * @throws ValidationException
     */
    public function update(Request $request)
    {
        $this->factory->validate($request->all(),[
            'latitude'  => 'required_with:longitude',
            'longitude'  => 'required_with:latitude'
        ]);
    }
}
