<?php

namespace Modules\Station\DataObjects;

use App\Common\BaseClasses\BaseDataObject;
use App\Common\Constants\TableNames;
use Illuminate\Http\Request;

/**
 *
 */
class DOListStation extends BaseDataObject
{
    /** @var ?int $company_id */
    private $company_id;
    /** @var ?double $latitude */
    private $latitude;
    /** @var ?double $longitude */
    private $longitude;
    /** @var ?int $radius */
    private $radius;
    /** @var ?array $companyIds */
    private $companyIds;
    /** @var ?bool $withCompany */
    private $withCompany;

    /**
     * @param Request $request
     * @return $this
     */
    public function prepareFromRequest(Request $request)
    {
        parent::prepareFromRequest($request);

        $data = $request->all();

        $distance_query = '';
        if (array_key_exists('latitude', $data) && array_key_exists('longitude', $data)) {
            // Here we select the distance
            $distance_query = ',(6371 * acos(cos(radians('.$data['latitude'].')) * ' .
            'cos(radians(stations.latitude)) * ' .
            'cos(radians(stations.longitude) - ' .
            'radians('.$data['longitude'].')) + ' .
            'sin(radians('.$data['latitude'].')) * ' .
            'sin(radians(stations.latitude)))) as distance';
        }

        $selectedCompanyColumns = '';
        if ($this->getWithCompany()) {
            $selectedCompanyColumns = ',' . TableNames::COMPANIES.'.name as company_name';
        }

        // if user enters lat and long so we will calculate distance of station with his desired (lat,long)
        $query = 'SELECT '.TableNames::STATIONS.'.name,'.TableNames::STATIONS.'.id,latitude,longitude,company_id'.$selectedCompanyColumns.$distance_query .
        ' FROM ' . TableNames::STATIONS;

        $groupBy = '';

        if ($this->getWithCompany()) {
            $query .= ' INNER JOIN ' . TableNames::COMPANIES;
            $query .= ' ON '.TableNames::STATIONS.'.company_id='.TableNames::COMPANIES.'.id';

            $groupBy = ' GROUP BY '.TableNames::STATIONS.'.id, ' . TableNames::COMPANIES.'.name';
        }

        $query .= ' WHERE true ';

        if ($this->getCompanyIds()) {
            $query .= 'AND company_id IN (' . implode(',', $this->getCompanyIds()) . ') ';
        }

        if ($distance_query) {
            $radius = array_key_exists('radius', $data) ? $data['radius'] : config('site.map_default_radius');
            $this->setLatitude($data['latitude']);
            $this->setLongitude($data['longitude']);
            $this->setRadius($radius);

            // Here we use distance on condition
            $query .= 'AND (6371 * acos(cos(radians('.$data['latitude'].')) * ' .
                'cos(radians(stations.latitude)) * ' .
                'cos(radians(stations.longitude) - ' .
                'radians('.$data['longitude'].')) + ' .
                'sin(radians('.$data['latitude'].')) * ' .
                'sin(radians(stations.latitude)))) <= ' . $radius;

            $query .= $groupBy;
            $query .= " ORDER BY distance ";
        } else {
            $query .= $groupBy;
        }

        $this->setRawQuery($query);

        return $this;
    }

    /**
     * @return int|null
     */
    public function getCompanyId(): ?int
    {
        return $this->company_id;
    }

    /**
     * @param int|null $company_id
     */
    public function setCompanyId(?int $company_id): void
    {
        $this->company_id = $company_id;
    }

    /**
     * @return float|null
     */
    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    /**
     * @param float|null $latitude
     */
    public function setLatitude(?float $latitude): void
    {
        $this->latitude = $latitude;
    }

    /**
     * @return float|null
     */
    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    /**
     * @param float|null $longitude
     */
    public function setLongitude(?float $longitude): void
    {
        $this->longitude = $longitude;
    }

    /**
     * @return int|null
     */
    public function getRadius(): ?int
    {
        return $this->radius;
    }

    /**
     * @param int|null $radius
     */
    public function setRadius(?int $radius): void
    {
        $this->radius = $radius;
    }

    /**
     * @return array|null
     */
    public function getCompanyIds(): ?array
    {
        return $this->companyIds;
    }

    /**
     * @param array|null $companyIds
     */
    public function setCompanyIds(?array $companyIds): void
    {
        $this->companyIds = $companyIds;
    }

    /**
     * @return bool|null
     */
    public function getWithCompany(): ?bool
    {
        return $this->withCompany;
    }

    /**
     * @param bool|null $withCompany
     */
    public function setWithCompany(?bool $withCompany): void
    {
        $this->withCompany = $withCompany;
    }
}
