<?php

namespace Modules\Station\Tests\Unit;

use App\Common\Constants\TableNames;
use Modules\Company\Entities\Company;
use Modules\Station\Actions\ActionGetStation;
use Modules\Station\Actions\ActionListStation;
use Modules\Station\Actions\ActionUpdateStation;
use Modules\Station\DataObjects\DOListStation;
use Modules\Station\Entities\Station;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 *
 */
class StationActionsTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testGetStation()
    {
        $company = Company::factory()->create();

        Station::factory()->for($company)->count(20)->create();

        $actionGetStation = resolve(ActionGetStation::class);
        $actionResult = $actionGetStation->__invoke(1);

        // Assert that getStationAction returns the company object
        $this->assertInstanceOf(Station::class, $actionResult);

        //Not necessary assertions
        $this->assertDatabaseCount(TableNames::STATIONS, 20);
    }

    /**
     * @return void
     */
    public function testListStation()
    {
        $company = Company::factory()->create();

        Station::factory()->for($company)->count(20)->create();

        $DOListStation = resolve(DOListStation::class);
        $DOListStation->prepareFromREquest(request());
        $actionListStation = resolve(ActionListStation::class);

        $list = $actionListStation->__invoke($DOListStation);

        $this->assertIsArray($list);
        $this->assertCount(20, $list);
    }

    /**
     * @return void
     */
    public function testUpdateStation()
    {
        $company = Company::factory()->create();
        $station = Station::factory()->for($company)->create();

        $stationArray = $station->toArray();
        $stationArray['name'] = 'new name';
        $actionUpdateStation = resolve(ActionUpdateStation::class);

        $result = $actionUpdateStation->__invoke($station->id, $stationArray);

        $this->assertTrue($result);
    }
}
