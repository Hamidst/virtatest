<?php

namespace Modules\Station\Tests\Unit;

use App\Common\Constants\TableNames;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Company\Entities\Company;
use Modules\Station\Entities\Station;
use Tests\TestCase;

class StationDatabaseTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testStationCanBeStored()
    {
        $station = Station::factory()->make()->toArray();
        $station['company_id']  = rand(1,100);
        Station::factory()->create($station);

        $this->assertDatabaseCount(TableNames::STATIONS, 1);
        $this->assertDatabaseHas(TableNames::STATIONS, $station);
    }

    public function testStationCompanyRelation()
    {
        $count = rand(1,10);

        $company = Company::factory()->hasStations($count)->create();


        $this->assertInstanceOf(HasMany::class, $company->stations());
        $this->assertCount($count, $company->stations->toArray());
    }
}
