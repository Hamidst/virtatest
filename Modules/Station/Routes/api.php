<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'station', 'middleware' => 'auth:api'], function () {
    Route::post('/', "HttpStoreStation");
});

Route::group(['prefix' => 'station'], function () {
    Route::get('/{id}', "HttpGetStation")->where('id', '[0-9]+');
    Route::put('/{id}', "HttpUpdateStation")->where('id', '[0-9]+');
    Route::get('/', "HttpListStation");
});
