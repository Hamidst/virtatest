<?php

namespace Modules\Station\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Modules\Station\Actions\ActionUpdateStation;
use Modules\Station\Exceptions\ExceptionStationNotFound;
use Modules\Station\Validators\ValidatorStation;

/**
 *
 */
class HttpUpdateStation
{
    /** @var ActionUpdateStation $actionUpdateStation */
    private $actionUpdateStation;
    /** @var ValidatorStation $validator */
    private $validator;

    /**
     * @param ActionUpdateStation $actionUpdateStation
     * @param ValidatorStation $validator
     */
    public function __construct(ActionUpdateStation $actionUpdateStation, ValidatorStation $validator)
    {
        $this->actionUpdateStation = $actionUpdateStation;
        $this->validator = $validator;
    }

    /**
     * @param int $stationId
     * @return JsonResponse
     * @throws ExceptionStationNotFound
     * @throws ValidationException
     */
    public function __invoke(int $stationId)
    {
        $this->validator->update(request());

        if (!$this->actionUpdateStation->__invoke($stationId, request()->all())) {
            throw new ExceptionStationNotFound();
        }

        return response()->json(['result' => true]);
    }
}
