<?php

namespace Modules\Station\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Modules\Station\Actions\ActionStoreStation;
use Modules\Station\Validators\ValidatorStation;

/**
 *
 */
class HttpStoreStation
{
    /** @var ActionStoreStation $actionStoreStation */
    private $actionStoreStation;
    /** @var ValidatorStation $validator */
    private $validator;

    /**
     * @param ActionStoreStation $actionStoreStation
     * @param ValidatorStation $validator
     */
    public function __construct(ActionStoreStation $actionStoreStation, ValidatorStation $validator)
    {
        $this->actionStoreStation = $actionStoreStation;
        $this->validator = $validator;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function __invoke(Request $request)
    {
        #1 Validation
        $this->validator->store($request);

        #2 Store to DB
        $station = $this->actionStoreStation->__invoke($request);

        #3 Return the stored station to user
        return response()->json($station->toArray());
    }
}
