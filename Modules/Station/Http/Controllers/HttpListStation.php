<?php

namespace Modules\Station\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Modules\Company\Actions\ActionGetCompany;
use Modules\Station\Actions\ActionListStation;
use Modules\Station\DataObjects\DOListStation;

/**
 *
 */
class HttpListStation
{
    /** @var ActionListStation $actionListStation */
    private $actionListStation;
    /** @var DOListStation $DOListStation */
    private $DOListStation;
    /** @var ActionGetCompany $actionGetCompany */
    private $actionGetCompany;

    /**
     * @param ActionListStation $actionListStation
     * @param DOListStation $DOListStation
     * @param ActionGetCompany $actionGetCompany
     */
    public function __construct(ActionListStation $actionListStation,
                                DOListStation $DOListStation,
                                ActionGetCompany $actionGetCompany)
    {
        $this->actionListStation = $actionListStation;
        $this->DOListStation = $DOListStation;
        $this->actionGetCompany = $actionGetCompany;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function __invoke(Request $request)
    {
        if ($request->get('company_id')) {
            $company_ids = $this->prepareCompanyIds($request->get('company_id'));
            $this->DOListStation->setCompanyIds($company_ids);
        }

        $this->DOListStation->setWithCompany(true);
        $this->DOListStation->prepareFromRequest($request);

        $stations = $this->actionListStation->__invoke(
            $this->DOListStation
        );

        return response()->json($stations);
    }

    /**
     * @param int $companyId
     * @return array
     * @description this function will return an array containing id of company and it's children. We use this list of
     * ids to fetch all the stations belonging to a company and it's children.
     */
    private function prepareCompanyIds(int $companyId) :array
    {
        $company = $this->actionGetCompany->__invoke($companyId, ['children']);

        $output[] = $company->id;

        foreach ($company->children as $id => $child) {
            if ($child->children)
                $output = array_merge($output, $this->prepareCompanyIds($child->id));
        }

        return $output;
    }
}
