<?php

namespace Modules\Station\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Modules\Station\Actions\ActionGetStation;
use Modules\Station\Exceptions\ExceptionStationNotFound;

/**
 *
 */
class HttpGetStation
{
    /** @var ActionGetStation $actionGetStation */
    private $actionGetStation;

    /**
     * @param ActionGetStation $actionGetStation
     */
    public function __construct(ActionGetStation $actionGetStation)
    {
        $this->actionGetStation = $actionGetStation;
    }

    /**
     * @param int $stationId
     * @return JsonResponse
     * @throws ExceptionStationNotFound
     */
    public function __invoke(int $stationId): JsonResponse
    {
        $station = $this->actionGetStation->__invoke($stationId);

        if (!$station) {
            throw new ExceptionStationNotFound();
        }

        return response()->json($station);
    }
}
