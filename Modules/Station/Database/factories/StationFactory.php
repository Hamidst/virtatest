<?php
namespace Modules\Station\Database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Modules\Station\Entities\Station;

class StationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Station::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'  => $this->faker->text,
            'latitude'  => $this->faker->latitude,
            'longitude' => $this->faker->longitude,
            'address'   => $this->faker->address
        ];
    }
}

