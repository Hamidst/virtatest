<?php

namespace Modules\Station\Database\Seeders;

use App\Common\Constants\TableNames;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class StationsTableTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table(TableNames::STATIONS)->insert([
            'name' => 'StationA1',
            'company_id' => 1,
            'latitude'  => '80.5',
            'longitude' => '31.5',
            'address'   => 'Address'
        ]);
        DB::table(TableNames::STATIONS)->insert([
            'name' => 'StationA2',
            'company_id' => 1,
            'latitude'  => 80.5,
            'longitude' => 31.5,
            'address'   => 'Address'
        ]);

        DB::table(TableNames::STATIONS)->insert([
            'name' => 'StationB1',
            'company_id' => 2,
            'latitude'  => 80.5,
            'longitude' => 31.5,
            'address'   => 'Address'
        ]);
        DB::table(TableNames::STATIONS)->insert([
            'name' => 'StationB2',
            'company_id' => 2,
            'latitude'  => 80.5,
            'longitude' => 31.5,
            'address'   => 'Address'
        ]);

        DB::table(TableNames::STATIONS)->insert([
            'name' => 'StationAA1',
            'company_id' => 3,
            'latitude'  => 80.5,
            'longitude' => 31.5,
            'address'   => 'Address'
        ]);

        DB::table(TableNames::STATIONS)->insert([
            'name' => 'StationAA2',
            'company_id' => 3,
            'latitude'  => 80.5,
            'longitude' => 31.5,
            'address'   => 'Address'
        ]);

        DB::table(TableNames::STATIONS)->insert([
            'name' => 'StatioBB1',
            'company_id' => 4,
            'latitude'  => 80.5,
            'longitude' => 31.5,
            'address'   => 'Address'
        ]);

        DB::table(TableNames::STATIONS)->insert([
            'name' => 'StatioBB2',
            'company_id' => 4,
            'latitude'  => 80.5,
            'longitude' => 31.5,
            'address'   => 'Address'
        ]);

    }
}
