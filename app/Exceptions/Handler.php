<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Modules\Company\Exceptions\BaseCompanyException;
use Modules\Company\Exceptions\Handler as CompanyHandler;
use Modules\Station\Exceptions\BaseStationException;
use Modules\Station\Exceptions\Handler as StationHandler;
use Modules\User\Exceptions\BaseUserException;
use Modules\User\Exceptions\Handler as UserHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->renderable(function (BaseUserException $e) {
            return app()->get(UserHandler::class)->render($e);
        });

        $this->renderable(function (BaseStationException $e) {
            return app()->get(StationHandler::class)->render($e);
        });

        $this->renderable(function (BaseCompanyException $e) {
            return app()->get(CompanyHandler::class)->render($e);
        });
    }
}
