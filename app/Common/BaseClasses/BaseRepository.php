<?php

namespace App\Common\BaseClasses;

use App\Common\Interfaces\IDataObject;
use App\Common\Interfaces\IRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 *
 */
abstract class BaseRepository implements IRepository
{
    /** @var Model $model */
    protected $model;

    /**
     * @param array $array
     * @return mixed
     */
    public function store(array $array)
    {
        return $this->model->newInstance()->create($array);
    }

    /**
     * @param int $id
     * @param array $array
     * @return bool|int
     */
    public function update(int $id, array $array)
    {
        $model = $this->model->newInstance()
            ->newQuery()
            ->find($id);

        if (!$model) {
            return false;
        }

        return $model->update($array);
    }

    /**
     * @param int $id
     * @return void
     */
    public function delete(int $id)
    {
        $this->model->newQuery()->find($id)->delete();
    }

    /**
     * @param int $id
     * @return Builder|Builder[]|Collection|Model|null
     */
    public function get(int $id)
    {
        $model = $this->model->newQuery()->find($id);
        if (!$model) {
            return null;
        }
        return $model;
    }

    /**
     * @param IDataObject $dataObject
     * @return LengthAwarePaginator|Builder[]|Collection
     */
    public function list(IDataObject $dataObject)
    {
        $query = $this->model->newQuery();

        if ($dataObject->getWhere()) {
            $query->where($dataObject->getWhere());
        }

        if ($dataObject->getWith()) {
            $query->with($dataObject->getWith());
        }

        if ($dataObject->getOrderBy()) {
            $query->orderBy($dataObject->getOrderBy(), $dataObject->getOrderDirection());
        }

        if ($dataObject->getRawQuery()) {
            $query->raw($dataObject->getRawQuery());
        }

        if (!$dataObject->getPaginated()) {
            return $query->get();
        }

        return $query->paginate(
            $dataObject->getPerPage(),
            '*',
            'Page',
            $dataObject->getPage()
        );
    }
}
