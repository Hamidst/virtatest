<?php

namespace App\Common\BaseClasses;

use App\Common\Interfaces\IDataObject;
use Illuminate\Http\Request;

abstract class BaseDataObject implements IDataObject
{
    /** @var ?int $id */
    private $id;
    /** @var bool $paginated */
    private $paginated;
    /** @var ?array $where */
    private $where;
    /** @var int $page */
    private $page;
    /** @var int $perPage */
    private $perPage;
    /** @var string $orderBy */
    private $orderBy;
    /** @var string $orderDirection */
    private $orderDirection;
    /** @var ?array $with */
    private $with;
    /** @var string $rawQuery */
    private $rawQuery;

    public function __construct()
    {
        $this->where = [];
    }

    public function prepareFromRequest(Request $request)
    {
        if ($request->get('page')) {
            $this->setPage($request->get('page'));
        } else {
            $this->setPage(1);
        }

        if ($request->get('perPage')) {
            $this->setPerPage($request->get('perPage'));
        } else {
            $this->setPerPage(config('site.list_default_per_page'));
        }

        if ($request->get('paginated')) {
            $this->setPaginated($request->get('paginated'));
        } else {
            $this->setPaginated(true);
        }

        if ($request->get('orderBy')) {
            $this->setOrderBy($request->get('orderBy'));
        } else {
            $this->setOrderBy('id');
        }

        if ($request->get('orderDirection')) {
            $this->setOrderDirection($request->get('orderDirection'));
        } else {
            $this->setOrderDirection('ASC');
        }

        if ($request->get('id')) {
            $this->setId($request->get('id'));
        }
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->setWhere(['id' => $id]);
        $this->id = $id;
    }

    /**
     * @return bool
     */
    public function getPaginated(): bool
    {
        return $this->paginated;
    }

    /**
     * @param bool $paginated
     */
    public function setPaginated(bool $paginated): void
    {
        $this->paginated = $paginated;
    }

    /**
     * @return array
     */
    public function getWhere(): ?array
    {
        return $this->where;
    }

    /**
     * @param array $where
     */
    public function setWhere(array $where): void
    {
        $this->where = array_merge($this->getWhere(), $where);
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getPerPage(): int
    {
        return $this->perPage;
    }

    /**
     * @param int $perPage
     */
    public function setPerPage(int $perPage): void
    {
        $this->perPage = $perPage;
    }

    /**
     * @return string
     */
    public function getOrderBy(): string
    {
        return $this->orderBy;
    }

    /**
     * @param string $orderBy
     */
    public function setOrderBy(string $orderBy): void
    {
        $this->orderBy = $orderBy;
    }

    /**
     * @return string
     */
    public function getOrderDirection(): string
    {
        return $this->orderDirection;
    }

    /**
     * @param string $orderDirection
     */
    public function setOrderDirection(string $orderDirection): void
    {
        $this->orderDirection = $orderDirection;
    }

    /**
     * @return array|null
     */
    public function getWith(): ?array
    {
        return $this->with;
    }

    /**
     * @param array|null $with
     */
    public function setWith(?array $with): void
    {
        $this->with = $with;
    }

    /**
     * @return string
     */
    public function getRawQuery(): ?string
    {
        return $this->rawQuery;
    }

    /**
     * @param string $rawQuery
     */
    public function setRawQuery(string $rawQuery): void
    {
        $this->rawQuery = $rawQuery;
    }
}
