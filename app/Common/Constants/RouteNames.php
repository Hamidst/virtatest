<?php

namespace App\Common\Constants;

class RouteNames
{
    const USER_REGISTER = 'register';
    const USER_LOGIN = 'login';
    const USER_LOGOUT = 'logout';
}
