<?php

namespace App\Common\Constants;

class TableNames
{
    const USERS = 'users';
    const COMPANIES = 'companies';
    const STATIONS = 'stations';
}
