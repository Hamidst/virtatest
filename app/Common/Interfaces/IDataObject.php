<?php

namespace App\Common\Interfaces;

use Illuminate\Http\Request;

interface IDataObject
{
    public function prepareFromRequest(Request $request);

    public function getWhere() :?array;

    public function getOrderBy() :string;

    public function getOrderDirection() :string;

    public function getPaginated() :bool;

    public function getPage() :int;

    public function getPerPage() :int;

    public function getWith() :?array;

    public function getRawQuery() :?string;
}
