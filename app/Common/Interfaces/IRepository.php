<?php

namespace App\Common\Interfaces;

use App\Common\BaseClasses\BaseDataObject;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

interface IRepository
{
    public function store(array $request);

    public function update(int $id, array $request);

    public function delete(int $id);

    public function get(int $id);

    public function list(iDataObject $dataObject);
}
