<?php
return [
    'list_default_per_page'     => env('LIST_DEFAULT_PER_PAGE') ?? 10,
    'map_default_latitude'      => env('MAP_DEFAULT_LATITUDE'),
    'map_default_longitude'      => env('MAP_DEFAULT_LONGITUDE'),
    'map_default_radius'      => env('MAP_DEFAULT_RADIUS')
];
