<h4>About project</h4>
This application is a technical assignment for Virta company.
I created this app's back-end using laravel on docker containers with Postgres database.
The authentication is done using Passport library so that we can use Oauth2 features.
In this project I have used laravel modules package so every package has it's own world consisting of repositories
and entities and even tests and so on.
We have 5 containers in this project which two of them are postgres and postgresTest (to run tests on it), I alsow created a pgadmin container so that you can easily manage databases.
A webserver container which is running nginx for us and the main app container.
I also have a simple .gitlab-ci.yml file in this project which runs a PHP_CodeSniffer command to check quality of codes in every commit using PSR2 standards.

<h4>Running the back-end app</h4>
To run project run:

```angular2html
docker-compose up -d
```
and bash into app container:
```angular2html
sudo docker-compose exec -u root app bash
```

then run:
```angular2html
cp .env.example .env
composer install
php artisan migrate
php artisan passport:install
```

There are three seeders to prepare some data for demo which can be run by running this command:
```angular2html
php artisan module:seed
```
This command will run all the modules seeders.

<h4>PostMan</h4>
Here is also the postman collection file in the root of the application (Virta Test.postman_collection.json), so you can import it into your postman app.
Please create two environment variable in postman with this key values:
base_url=http://0.0.0.0:8081/api/
token=AuthToken you can get by calling login endpoint

![](./ReadmeFiles/postman1.png)

To login and get token after running register user endpoint. Call login endpoint using registered user's email and password.
Then copy the token in response into the token environment variable of postman collection.

![](./ReadmeFiles/postman2.png)

Then you can easily call any endpoint

I have 17 simple tests in this project, and I have to mention that I am familiar with Mockery to make more professional tests. 

<h4>Front-end app</h4>
In order to simpy use this project you can use this front-end project which is based on react.js.
https://gitlab.com/Hamidst/virtatestfront/-/tree/master

Here is some description about front-end app too:
After cloning this repository please go into the project folder and run following commands:
```angular2html
npm i
npm run start
```

Then you can see the project running on http://localhost:3000 and you should see home page as follow.

![](./ReadmeFiles/fton1.png)

After you press login you can see the login page and be able to login with this information:
email: hamid.gholami@devolon.fi
pass: 123456

![](./ReadmeFiles/front2.png)

After login you will be redirected to profile page and there you can add new companies and stations.

We will have 4 Companies which CompanyA is parent of CompanyAA and CompanyB is parent of CompanyBB.
Each company has two direct stations which belongs to themselves, But parent companies have their children's stations which you can see in seperate columns.
Please feel free to add new companies and new stations for them, in any page by clicking on Hamid name on top right you will be redirected to profile page.

![](./ReadmeFiles/front3.png)

You can see each company's direct stations and it's children's stations seperated in a new page if you click on stations link in front of each company.

![](./ReadmeFiles/front4.png)
